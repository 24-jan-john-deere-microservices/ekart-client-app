package com.classpath.demo.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/token")
@Slf4j
@RequiredArgsConstructor
public class UserController {

    private final OAuth2AuthorizedClientService oAuth2AuthorizedClientService;

    @GetMapping
    public Map<String, String> printUserDetails(OAuth2AuthenticationToken oAuth2AuthenticationToken){
        log.info(" Token :: ");
        OAuth2User principal = oAuth2AuthenticationToken.getPrincipal();
        OAuth2AuthorizedClient oAuth2AuthorizedClient = this.oAuth2AuthorizedClientService
                                                                .loadAuthorizedClient(oAuth2AuthenticationToken.getAuthorizedClientRegistrationId(), oAuth2AuthenticationToken.getName());

        OAuth2AccessToken accessToken = oAuth2AuthorizedClient.getAccessToken();
        String tokenValue = accessToken.getTokenValue();
        Map<String,String> userInfo = new HashMap<>();
        userInfo.put("Token ", tokenValue);
        userInfo.put("Scopes", oAuth2AuthorizedClient.getAccessToken().getScopes().toString());
        return userInfo;

    }
}